﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADIRoadShow.Utility.Cmind
{
    public class HtmlHelper
    {
        public static List<SelectListItem> GetSelectedOptions(List<SelectListItem> source, string removeValue, string selectedValue)
        {
            var result = source.Where(p => p.Value != removeValue)
                .Select(p => new SelectListItem()
                {
                    Text = p.Text,
                    Value = p.Value,
                    Selected = p.Value == selectedValue
                });

            return result.ToList();
        }
    }
}