﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace ADIRoadShow.Utility.Cmind
{
    public class MailHelper
    {
        private static readonly string senderMail = ConfigurationManager.AppSettings["SenderMail"];
        private static readonly string senderDisplayName = ConfigurationManager.AppSettings["SenderDisplayName"];

        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static bool SendEmail(MailBase mailBase)
        {
            MailAddress from;
            if (!string.IsNullOrEmpty(senderDisplayName))
            {
                from = new MailAddress(senderMail, senderDisplayName, System.Text.Encoding.UTF8);
            }
            else
            {
                from = new MailAddress(senderMail);
            }

            SmtpClient smtpMail = new SmtpClient();
            smtpMail.EnableSsl = false;

            bool sendMailResult = sendEmail(smtpMail, from, mailBase);
            logger.Info(string.Format("Send Mail Result: {0}, mail: {1}",
                sendMailResult, mailBase.Email));
            return sendMailResult;
        }

        private static bool sendEmail(SmtpClient client, MailAddress from, MailBase mailBase)
        {
            bool result = true;
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = from;
                string[] mail = mailBase.Email.Trim().Split(",".ToCharArray());
                foreach (string reciver in mail)
                {
                    mailMessage.To.Add(reciver.Trim());
                }
                //StringBuilder str = new StringBuilder();
                //str.AppendLine("BEGIN:VCALENDAR");
                //str.AppendLine("PRODID:-//Schedule a Meeting");
                //str.AppendLine("VERSION:2.0");
                //str.AppendLine("METHOD:REQUEST");
                //str.AppendLine("BEGIN:VEVENT");
                //str.AppendLine("DTSTART:20211201T053000Z");
                //str.AppendLine("DTEND:20211231T1700Z");
                //str.AppendLine("DTSTAMP:20211130T083008Z");
                ////str.AppendLine("LOCATION: " + "台北市中山區樂群二路55號");
                //str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                //str.AppendLine(string.Format("DESCRIPTION:{0}", "2021 ADI 應用科技展 https://adievent2021.com/"));
                //str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", mailMessage.Body));
                //str.AppendLine(string.Format("SUMMARY:{0}", "2021 ADI 應用科技展"));
                //str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", mailMessage.From.Address));

                //str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", mailMessage.To[0].DisplayName, mailMessage.To[0].Address));

                //str.AppendLine("BEGIN:VALARM");
                //str.AppendLine("TRIGGER:-PT15M");
                //str.AppendLine("ACTION:DISPLAY");
                //str.AppendLine("DESCRIPTION:Reminder");
                //str.AppendLine("END:VALARM");
                //str.AppendLine("END:VEVENT");
                //str.AppendLine("END:VCALENDAR");

                //byte[] byteArray = Encoding.ASCII.GetBytes(str.ToString());
                //MemoryStream stream = new MemoryStream(byteArray);

                //ContentType contype = new ContentType("text/calendar");
                //contype.Parameters.Add("method", "REQUEST");
                //AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), contype);
                //mailMessage.AlternateViews.Add(avCal);
                var file = @"C:\inetpub\wwwroot\adievent2021.com\invite.ics";
                Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
                ContentDisposition disposition;
                disposition = data.ContentDisposition;
                disposition.CreationDate = System.IO.File.GetCreationTime(file);
                disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
                disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
                mailMessage.Attachments.Add(data);//新增附件

                mailMessage.Subject = mailBase.Subject;
                StringBuilder strBody = new StringBuilder();
                strBody.Append(mailBase.MailBody);
                mailMessage.Body = strBody.ToString();
                mailMessage.IsBodyHtml = true;
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                var json = new JavaScriptSerializer().Serialize(mailBase);
                string msg = string.Format(
                    "Send Mail False, mail:{0}, errorMsg:{1}",
                    json,
                    ex.Message);
                logger.Error(msg);
                result = false;
            }
            return result;
        }
    }

    public class MailBase
    {
        public virtual string Email { get; set; }

        public virtual string Subject { get; set; }

        public virtual string MailBody { get; set; }
    }
}