﻿using ADIRoadShow.Models.Cmind;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace ADIRoadShow.Utility.Cmind
{
    public class MemberInfoHelper
    {
        /// <summary>
        /// 存放資料的Key
        /// </summary>
        private const string CMIND_MEMBER = "ADIRoadShowMember2";

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="memberInfo"></param>
        /// <param name="isRemeber"></param>
        public static void Login(MemberInfo memberInfo, bool isRemeber)
        {
            CookieHelper.Del(CMIND_MEMBER);
            SessionHelper.Del(CMIND_MEMBER);
            string data = JsonConvert.SerializeObject(memberInfo);
            //var cookieText = Encoding.UTF8.GetBytes(data);
            //var encryptedValue = Convert.ToBase64String(MachineKey.Protect(cookieText, "ProtectCookie"));
            //if (isRemeber)
            //{
            DateTime expires = DateTime.Now.AddDays(1);
            CookieHelper.SetCookie(CMIND_MEMBER, data, expires);
            //}
            //else
            //{
            SessionHelper.SetSession(CMIND_MEMBER, data);
            //}
        }

        /// <summary>
        /// 登出
        /// </summary>
        public static void Logout()
        {
            SessionHelper.Del(CMIND_MEMBER);
            CookieHelper.Del(CMIND_MEMBER);
        }

        /// <summary>
        /// 取得會員資料
        /// </summary>
        /// <returns></returns>
        public static MemberInfo GetMemberInfo()
        {
            //CookieHelper.Del(CMIND_MEMBER);

            //string data = SessionHelper.Get(CMIND_MEMBER);
            string data = CookieHelper.Get(CMIND_MEMBER);


            if (!string.IsNullOrEmpty(data))
            {
                //var encryptedValue = Convert.FromBase64String(data);
                //var output = MachineKey.Unprotect(encryptedValue, "ProtectCookie");
                //var cookieText = Encoding.UTF8.GetString(output);
                MemberInfo memberInfo = JsonConvert.DeserializeObject<MemberInfo>(data);
                return memberInfo;
            }

            return null;
        }

        /// <summary>
        /// 會員是否為登入狀態
        /// </summary>
        /// <returns></returns>
        public static bool IsLogin()
        {
            MemberInfo memberInfo = GetMemberInfo();
            return memberInfo != null;
        }
    }
}