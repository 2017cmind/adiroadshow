﻿using ADIRoadShow.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ADIRoadShow.ActionFilters
{
    /// <summary>
    /// 前台會員登入驗證
    /// </summary>
    public class MemberAuthorize : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isLogin = MemberInfoHelper.IsLogin();
            //CookieHelper.SetCookie("RetrunUrl", "");
            if (filterContext.HttpContext.Request.Url.PathAndQuery.IndexOf("VR") != -1 || filterContext.HttpContext.Request.Url.PathAndQuery.IndexOf("Video") != -1)
            {
                CookieHelper.SetCookie("RetrunUrl", filterContext.HttpContext.Request.Url.PathAndQuery);
            }
            if (!isLogin)
            {

                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        { "controller", "Login" },
                        { "action", "Index" }
                    });
            }
            base.OnActionExecuting(filterContext);
        }
    }
}