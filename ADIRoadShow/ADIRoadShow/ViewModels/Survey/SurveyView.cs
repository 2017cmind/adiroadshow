﻿using ADIRoadShow.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ADIRoadShow.ViewModels.Survey
{
    public class SurveyView
    {
        public int ID { get; set; }
        public int ShowRoomType { get; set; }

        [Required]
        public string Name { get; set; }
        public Dealer ? DealerId { get; set; }    
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Company { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public bool IsKnowing { get; set; }

        public string Interest { get; set; }
        [Required]
        public string Project { get; set; }

        public string ProjectTxt { get; set; }

        public string InformationTxt { get; set; }

        [Required]
        public string Information { get; set; }

        /// <summary>
        /// check list
        /// </summary>
        public List<string> groups { get; set; }

        public System.DateTime CreateTime { get; set; }
    }
}