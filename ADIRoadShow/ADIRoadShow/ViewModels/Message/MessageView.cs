﻿using ADIRoadShow.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ADIRoadShow.ViewModels.Message
{
    public class MessageView
    {
        public IEnumerable<MessageView> DataList { get; set; }
        public int ID { get; set; }
        public string NickName { get; set; }

        public ShowRoomType ShowRoomType { get; set; }

        [Required]
        public string Content { get; set; }

        public int ApplyId { get; set; }

        public bool IsAdmin { get; set; }

        public int MessageId { get; set; }
        public bool IsLock { get; set; }
        public System.DateTime CreateTime { get; set; }
    }
}