﻿using ADIRoadShow.Models;
using ADIRoadShow.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADIRoadShow.ViewModels.Apply
{
    public class Group
    {
        public string Name { get; set; }
    }
    public class ApplyView
    {
       
        public ApplyView()
        {
            //And some data to play with
            var allGroups = new List<Group>();
            var values = EnumHelper.GetValues<EventInformationType>();

            foreach (var v in values)
            {
                allGroups.Add(new Group
                {
                    Name = EnumHelper.GetDescription(v),
                });
            }
            this.CheckOptions = allGroups;
        }
        public int ID { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "名")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "姓")]
        public string LastName { get; set; }

        public int DealerId { get; set; }

        [Required]
        [Display(Name = "暱稱")]
        public string NickName { get; set; }

        [Required]
        [RegularExpression(@"^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$", ErrorMessage = "Email格式錯誤 範例: AAA@gmail.com！")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^[09]{2}[0-9]{8}$", ErrorMessage = "行動電話ex:0912345678")]
        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "產業分類")]
        public string Category { get; set; }

        [Display(Name = "產業分類")]
        [StringLength(20, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string CategoryTxt { get; set; }

        public string Url { get; set; }

        [Required]
        [Display(Name = "職業分類")]
        public string JobCategory { get; set; }

        /// <summary>
        /// check list
        /// </summary>
        public List<string> groups { get; set; }

        public List<Group> CheckOptions { get; set; }
        [Required]
        [Display(Name = "公司")]
        public string Company { get; set; }

        [Display(Name = "職位")]
        public string Position { get; set; }

        [Display(Name = "部門")]
        public string Department { get; set; }

        [Display(Name = "取得活動資訊管道")]
        public string EventInformation { get; set; }

        [Display(Name = "封鎖")]
        public bool IsLock { get; set; }

        public List<SelectListItem> JobCategoryOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<JobCategory>();
                result.Insert(0, new SelectListItem { Text = "請選擇", Value = "" });

                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = EnumHelper.GetDescription(v),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}