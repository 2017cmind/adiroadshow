﻿using ADIRoadShow.ViewModels.Apply;
using ADIRoadShow.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Models.MailTemplate
{
    public class MailSuccessTemplate : MailBase
    {
        private const string SUBJECT = "[報名成功]2021 ADI應用科技展活動";

        //寄信內容文字
        private const string template = @"<table border='0' cellpadding='0' cellspacing='0' class='nl-container' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0;background-color:#000' width='100%'> <tbody> <tr> <td> <table align='center' border='0' cellpadding='0' cellspacing='0' class='row row-2' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0;background-color:#f6f6f6' width='100%'> <tbody> <tr> <td> <table align='center' border='0' cellpadding='0' cellspacing='0' class='row-content stack' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0;background-color:#131836;' width='650'> <tbody> <tr> <th class='column' style='mso-table-lspace:0;mso-table-rspace:0;font-weight:400;text-align:left;vertical-align:top' width='100%'> <table border='0' cellpadding='0' cellspacing='0' class='divider_block' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0' width='100%'> </table> <table border='0' cellpadding='20' cellspacing='0' class='image_block' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0' width='100%'> <tr> <div align='center' style='line-height:10px'><img alt='Forgot your password?' src='{1}/Content/analog/images/lock4.jpg' style='display:block;height:auto;border:0;width:700px;max-width:100%' title='Forgot your password?' width='325'/></div></tr></table> <table border='0' cellpadding='0' cellspacing='0' class='heading_block' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0' width='100%'> <tr> <td style='padding-top:35px;text-align:center;width:100%;color:#fff'><h1 style='margin:0;color:#fff;direction:ltr;font-family:Cabin,Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:28px;font-weight:400;letter-spacing:normal;line-height:120%;text-align:center;margin-top:0;margin-bottom:0'><strong>HI {0}, 恭喜您已經成功報名2021 ADI應用科技展活動！ </strong></h1></td></tr></table> <table border='0' cellpadding='0' cellspacing='0' class='text_block' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0;word-break:break-word' width='100%'> <tr> <td style='padding-left:45px;padding-right:45px;padding-top:10px'> <div style='font-family:Arial,sans-serif'> <div style='font-size:12px;font-family:Cabin,Arial,'Helvetica Neue',Helvetica,sans-serif;color:#393d47;line-height:1.5'> <p style='margin:0;text-align:center;mso-line-height-alt:27px'><span style='font-size:18px;color:#fff;letter-spacing: 0.1rem;'>線上展覽會活動將於12/01(三)～12/31(五)展開，請於活動當天點選活動網址，並輸入您的帳號（Email）和密碼（手機號碼）即可登入，最新的科技資訊即將等著您！</span></p></div></div></td></tr></table> <table border='0' cellpadding='0' cellspacing='0' class='text_block' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0;word-break:break-word;' width='100%'> <tr> <td style='padding-left:45px;padding-right:45px;padding-top:10px'> <div style='font-family:Arial,sans-serif'> <div style='font-size:12px;font-family:Cabin,Arial,'Helvetica Neue',Helvetica,sans-serif;text-align:center;color:#393d47;line-height:1.5'> <p style='margin:0;mso-line-height-alt:19.5px;text-align:center'> <span style='font-size:16px;color:#fff'> <strong>活動時間</strong><br>線上展覽會 | 12/1(三)至12/31(五) </span> </p><p style='margin:0;mso-line-height-alt:19.5px;text-align:center'> <span style='font-size:16px;color:#fff'> <strong> 活動聯絡信箱 </strong> | <a href='mailto:service@shephedad.com' style='font-size:20px;color:#fff'>service@shephedad.com</a> </span> </p><p style='margin:0;mso-line-height-alt:19.5px;text-align:center'> <span style='font-size:16px;color:#fff'> <strong> (此為系統自動寄信，請勿回覆此信) </strong> </p></div></div></td></tr></table> <table border='0' cellpadding='20' cellspacing='0' class='divider_block' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0' width='100%'> <tr> <td> <div align='center'> <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0' width='80%'> <tr> <td class='divider_inner' style='font-size:1px;line-height:1px;border-top:1px solid #cfcfcf'><span></span></td></tr></table> </div></td></tr></table> <table border='0' cellpadding='20' cellspacing='0' class='image_block' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0' width='100%'> <tr> <div align='center' style='line-height:10px'><img alt='Forgot your password?' src='{1}/Content/analog/images/lock5.png' style='display:block;height:auto;border:0;width:700px;max-width:100%' title='Forgot your password?' width='325'/></div></tr></table> <table border='0' cellpadding='20' cellspacing='0' class='image_block' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0' width='100%'> <tr> <div align='center' style='line-height:10px'><img alt='Forgot your password?' src='{1}/Content/analog/images/lock6.jpg' style='display:block;height:auto;border:0;width:700px;max-width:100%' title='Forgot your password?' width='325'/></div></tr></table> <table border='0' cellpadding='20' cellspacing='0' class='image_block' role='presentation' style='mso-table-lspace:0;mso-table-rspace:0' width='100%'> <tr> <div align='center' style='line-height:10px'><img alt='Forgot your password?' src='{1}/Content/analog/images/lock7.jpg' style='display:block;height:auto;border:0;width:700px;max-width:100%' title='Forgot your password?' width='325'/></div></tr></table> </th> </tr></tbody> </table> </td></tr></tbody> </table> </td></tr></tbody> </table>";
        /// <summary>
        /// 寄信資訊
        /// </summary>
        public MailSuccessTemplate(ApplyView model)
        {
            this.Email = model.Email;
            //主旨
            this.Subject = SUBJECT;
            //寄信內容
            this.MailBody = getMailBody(model);
        }

        /// <summary>
        /// 取得寄信內容參數
        /// </summary>
        /// <returns></returns>
        private string getMailBody(ApplyView model)
        {
            //寄信內容參數
            string mailBody = string.Format(template,
                model.LastName + model.FirstName,
                model.Url
            );
            //string result = @"<!DOCTYPE html>
            //                <html lang='en' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>
            //                <head>
            //                    <title></title>
            //                    <meta charset='utf-8' />
            //                    <meta content='width=device-width,initial-scale=1' name='viewport' />
            //                    <!--[if mso]><xml><o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch><o:AllowPNG/></o:OfficeDocumentSettings></xml><![endif]--><!--[if !mso]><!-->
            //                    <link href='https://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css' />
            //                    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css' />
            //                    <!--<![endif]-->
            //                    <style>
            //                * {
            //                 box-sizing: border-box
            //                }
            //                th.column {
            //                 padding: 0
            //                }
            //                a[x-apple-data-detectors] {
            //                 color: inherit!important;
            //                 text-decoration: inherit!important
            //                }
            //                #MessageViewBody a {
            //                 color: inherit;
            //                 text-decoration: none
            //                }
            //                p {
            //                 line-height: inherit
            //                }

            //                @media (max-width:670px) {
            //                .icons-inner {
            //                 text-align: center
            //                }
            //                .icons-inner td {
            //                 margin: 0 auto
            //                }
            //                .row-content {
            //                 width: 100%!important
            //                }
            //                .stack .column {
            //                 width: 100%;
            //                 display: block
            //                }
            //                }
            //                    </style>
            //                </head>
            //                <body style='background-color:#000;margin:0;padding:0;-webkit-text-size-adjust:none;text-size-adjust:none'>"
            //                + mailBody 
            //                +" </body></html>";
            string result = mailBody;
            return result;
        }
    }
}