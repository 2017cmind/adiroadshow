﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Models
{
    /// <summary>
    /// 產業類型
    /// </summary>
    public enum CareerType
    {
        /// <summary>
        /// 工業自動化及能源採集(AEG)
        /// </summary>
        [Description("工業自動化及能源採集(AEG)")]
        AEG = 1,

        /// <summary>
        /// 軍事及航太工業(ASD)      
        /// </summary>
        [Description("軍事及航太工業(ASD)")]
        ASD = 2,

        /// <summary>
        /// 汽車相關產業(AUT)
        /// </summary>
        [Description("汽車相關產業(AUT)")]
        AUT = 3,

        /// <summary>
        /// 通訊產業(COM)        
        /// </summary>
        [Description("通訊產業(COM)")]
        COM = 4,

        /// <summary>
        /// CON       
        /// </summary>
        [Description("消費電子產業(CON)")]
        CON = 5,

        /// <summary>
        /// 數位醫療產業(DHC)
        /// </summary>
        [Description("數位醫療產業(DHC)")]
        DHC = 6,

        /// <summary>
        /// 儀器儀表設備產(INS)      
        /// </summary>
        [Description("儀器儀表設備產(INS)")]
        INS = 7,

        /// <summary>
        /// 其他      
        /// </summary>
        [Description("其他")]
        Others = 8,

    }

    /// <summary>
    /// 工作職位類型
    /// </summary>
    public enum JobCategory
    {
        /// <summary>
        /// 系統設計工程師
        /// </summary>
        [Description("系統設計工程師")]
        SystemEngineer = 1,

        /// <summary>
        /// 硬體設計工程師       
        /// </summary>
        [Description("硬體設計工程師")]
        HardwareEngineer = 2,

        /// <summary>
        /// 韌體設計工程師       
        /// </summary>
        [Description("韌體設計工程師")]
        FirmwareEngineer = 3,

        /// <summary>
        /// 嵌入式設計工程師      
        /// </summary>
        [Description("嵌入式設計工程師")]
        EmbeddedEngineer = 4,

        /// <summary>
        /// 總工程師      
        /// </summary>
        [Description("總工程師")]
        ChiefEngineer = 5,

        /// <summary>
        /// 品質或元件工程師     
        /// </summary>
        [Description("品質或元件工程師")]
        QualityAndTestingEngineer = 6,

        /// <summary>
        /// 軟體工程師      
        /// </summary>
        [Description("軟體工程師")]
        SoftWareEngineer = 7,

        /// <summary>
        /// 專案經理/總監      
        /// </summary>
        [Description("專案經理/總監")]
        ProjectManager = 8,

        /// <summary>
        /// 採購商      
        /// </summary>
        [Description("採購商")]
        Purchasing = 9,

        /// <summary>
        /// 代理商      
        /// </summary>
        [Description("代理商")]
        Agent = 10,

        /// <summary>
        /// 大學生      
        /// </summary>
        [Description("大學生")]
        Student = 11,

        /// <summary>
        /// 教授/助理教授      
        /// </summary>
        [Description("教授/助理教授")]
        Professor = 12,

        /// <summary>
        /// 助理研究員、講師、或導師     
        /// </summary>
        [Description("助理研究員、講師、或導師")]
        Tutor = 13,

        /// <summary>
        /// 其他      
        /// </summary>
        [Description("其他")]
        Others = 14

    }
    /// <summary>
    /// 工作職位類型
    /// </summary>
    public enum EventInformationType
    {
        /// <summary>
        /// ADI電子報
        /// </summary>
        [Description("ADI電子報")]
        Newsletter = 1,

        /// <summary>
        /// ADIFB粉絲專業       
        /// </summary>
        [Description("ADIFB粉絲專業  ")]
        FB = 2,

        /// <summary>
        /// 安馳科技       
        /// </summary>
        [Description("安馳科技")]
        Anstek = 3,

        /// <summary>
        /// 艾睿電子      
        /// </summary>
        [Description("艾睿電子")]
        Arrow = 4,

        /// <summary>
        /// 茂宣企業      
        /// </summary>
        [Description("茂宣企業")]
        Morrihan = 5,

        /// <summary>
        /// 貿澤電子     
        /// </summary>
        [Description("貿澤電子")]
        Mouser = 6,

        /// <summary>
        /// 朋友介紹      
        /// </summary>
        [Description("朋友介紹")]
        Friend = 7,

        /// <summary>
        ///社群媒體
        /// </summary>
        [Description("社群媒體")]
        SocialMedia = 8,
    }

    /// <summary>
    /// 管理員類別
    /// </summary>
    public enum AdminType
    {
        /// <summary>
        /// 一般管理員
        /// </summary>
        [Description("一般管理員")]
        Admins = 0,

        /// <summary>
        /// 系統管理員       
        /// </summary>
        [Description("系統管理員")]
        Managers = 1,
    }

    /// <summary>
    /// 虛擬展覽
    /// </summary>
    public enum ShowRoomType
    { 
        /// <summary>
        /// VR
        /// </summary>
        [Description("VR")]
        VR = 1,

        /// <summary>
        /// 線上活動       
        /// </summary>
        [Description("線上活動")]
        Video = 2,
    }
    /// <summary>
    /// 經銷商
    /// </summary>
    public enum Dealer
    {
        /// <summary>
        /// 一般管理員
        /// </summary>
        [Description("無")]
        None = 0,
        /// <summary>
        /// Adi
        /// </summary>
        [Description("Adi")]
        Adi = 1,

        /// <summary>
        /// Anstek       
        /// </summary>
        [Description("Anstek")]
        Anstek = 2,

        /// <summary>
        /// Arrow       
        /// </summary>
        [Description("Arrow")]
        Arrow = 3,

        /// <summary>
        /// Morrihan       
        /// </summary>
        [Description("Morrihan")]
        Morrihan = 4,

        /// <summary>
        /// Mouser       
        /// </summary>
        [Description("Mouser")]
        Mouser = 5,
    }
    /// <summary>
    /// 線上展覽類別
    /// </summary>
    public enum ExhibitionCategory
    {
        [Description("無")]
        None = 0,
        /// <summary>
        /// 震動分析與資料擷取量測平台的設計經驗分享
        /// </summary>
        [Description("震動分析與資料擷取量測平台的設計經驗分享")]
        Exhibition1 = 1,

        /// <summary>
        /// 水質監控系統       
        /// </summary>
        [Description("水質監控系統")]
        Exhibition2 = 2,

        /// <summary>
        /// 第3代無線狀態監控模組應用方案       
        /// </summary>
        [Description("第3代無線狀態監控模組應用方案")]
        Exhibition3 = 3,

        /// <summary>
        /// 回音消除演算法應用方案       
        /// </summary>
        [Description("回音消除演算法應用方案")]
        Exhibition4 = 4,

        /// <summary>
        /// 新一代高畫質多媒體介面 (HDMI)產品應用設計經驗分享       
        /// </summary>
        [Description("新一代高畫質多媒體介面 (HDMI)產品應用設計經驗分享")]
        Exhibition5 = 5,

        /// <summary>
        /// 新一代 3D深度感測器產品應用設計經驗分享       
        /// </summary>
        [Description("新一代 3D深度感測器產品應用設計經驗分享")]
        Exhibition6 = 6
    }

    /// <summary>
    /// 興趣
    /// </summary>
    public enum InterestType
    {
        /// <summary>
        /// 回音消除演算法應用方案 (AEC)
        /// </summary>
        [Description("回音消除演算法應用方案 (AEC)")]
        AEC = 1,

        /// <summary>
        /// 3D ToF 智慧視覺應用方案       
        /// </summary>
        [Description("3D ToF 智慧視覺應用方案")]
        ToF3D = 2,

        /// <summary>
        /// IOT安全性解決方案       
        /// </summary>
        [Description("IOT安全性解決方案")]
        IOT = 3,

        /// <summary>
        /// 新一代高畫質多媒體介面解決方案 (HDMI 2.1)       
        /// </summary>
        [Description("新一代高畫質多媒體介面<br>解決方案(HDMI 2.1)")]
        HDMI = 4,

        /// <summary>
        /// 人數統計系統在智慧城市應用方案 (Eagle eye) 
        /// </summary>
        [Description("人數統計系統在智慧城市<br>應用方案 (Eagle eye) ")]
        EagleEye = 5,

        /// <summary>
        /// 啟用物聯網人工智慧帶有神經網路加速器的低功耗微控制器 MAX78000    
        /// </summary>
        [Description("啟用物聯網人工智慧，<br>帶有神經網路加速器的低功耗<br>微控制器 MAX78000")]
        MAX78000 = 6,

        /// <summary>
        /// 資料擷取與震動量測分析應用方案(DAQ + CbM)
        /// </summary>
        [Description("資料擷取與震動量測分析<br>應用方案(DAQ + CbM)")]
        DAQCBM = 7,

        /// <summary>
        /// 設備儀器故障AI預防診斷系統(AI + CbM)
        /// </summary>
        [Description("設備儀器故障AI預防<br>診斷系統(AI + CbM)")]
        AICBM = 8,

        /// <summary>
        /// 第3代無線狀態監控模組應用方案 (Voyager)
        /// </summary>
        [Description("第3代無線狀態監控模組<br>應用方案(Voyager)")]
        Voyager = 9,

        /// <summary>
        ///水質監控系統
        /// </summary>
        [Description("水質監控系統")]
        Water = 10,

        /// <summary>
        /// 5G通訊網路 - 無線射頻平台應用方案
        /// </summary>
        [Description("5G通訊網路 - 無線射頻平台<br>應用方案")]
        Internet5G = 11,

        /// <summary>
        /// 新一代3D深度感測器應用方案
        /// </summary>
        [Description("新一代3D深度感測器應用方案")]
        Sensor = 12,
    }

    /// <summary>
    /// 專案項目類別
    /// </summary>
    public enum ProjectType
    {
        /// <summary>
        /// 系統整合服務
        /// </summary>
        [Description("系統整合服務")]
        System = 1,

        /// <summary>
        /// 硬體開發       
        /// </summary>
        [Description("硬體開發")]
        HardWare = 2,

        /// <summary>
        /// 策略行銷       
        /// </summary>
        [Description("策略行銷")]
        Marketing = 3,

        /// <summary>
        /// 教育學習       
        /// </summary>
        [Description("教育學習")]
        Educate = 4,

        /// <summary>
        /// 其他      
        /// </summary>
        [Description("其他")]
        Others = 5
    }
    /// <summary>
    ///訊息服務類別
    /// </summary>
    public enum InformationType
    {
        /// <summary>
        /// 專人聯繫
        /// </summary>
        [Description("專人聯繫")]
        Contact = 1,

        /// <summary>
        /// 樣品/EV Board申請       
        /// </summary>
        [Description("樣品/EV Board申請")]
        Sample = 2,

        /// <summary>
        /// 產品訊息       
        /// </summary>
        [Description("產品訊息")]
        Product = 3,

        /// <summary>
        /// 其他      
        /// </summary>
        [Description("其他")]
        Others = 4
    }
}