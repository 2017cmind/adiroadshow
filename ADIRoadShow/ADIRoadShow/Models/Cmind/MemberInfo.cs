﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Models.Cmind
{
    public class MemberInfo
    {
        public int ID { get; set; }

        public bool IsAdmin { get; set; }

        public Dealer ? DealerId { get; set; }
    }
}