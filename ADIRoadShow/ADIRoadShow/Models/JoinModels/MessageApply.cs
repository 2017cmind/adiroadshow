﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Models.JoinModels
{
    public class MessageApply
    {
        public int ID { get; set; }
        public string NickName { get; set; }
        public string Content { get; set; }
        public int ShowRoomType { get; set; }
        public int ExhibitionType { get; set; }
        public int MessageId { get; set; }
        public bool IsLock { get; set; }
        public bool IsAdmin { get; set; }
        public string Name { get; set; }
        public int ApplyId { get; set; }
        public System.DateTime CreateTime { get; set; }
    }
}