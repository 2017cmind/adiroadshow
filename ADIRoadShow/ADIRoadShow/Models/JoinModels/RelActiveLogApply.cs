﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Models.JoinModels
{
    public class RelActiveLogApply
    {
        public int ID { get; set; }
        public int ApplyId { get; set; }
        public ShowRoomType ShowRoomType { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Company { get; set; }
        public ExhibitionCategory ExhibitionType { get; set; }
        public Dealer? DealerId { get; set; }
        public DateTime? OnlineTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}