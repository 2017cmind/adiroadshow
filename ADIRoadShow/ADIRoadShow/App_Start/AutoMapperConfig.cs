﻿using ADIRoadShow.Models;
using ADIRoadShow.Models.JoinModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADIRoadShow.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                #region 後台
                cfg.CreateMap<Apply, Areas.Admin.ViewModels.Apply.ApplyView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Apply.ApplyView, Apply>();
                cfg.CreateMap<RelMessageApply, Areas.Admin.ViewModels.Message.MessageView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Message.MessageView, Message>();
                cfg.CreateMap<Message, Areas.Admin.ViewModels.Message.MessageView>();
                cfg.CreateMap<RelActiveLogApply, Areas.Admin.ViewModels.ActiveLog.ActiveLogView>();
                cfg.CreateMap<ActiveLog, Areas.Admin.ViewModels.ActiveLog.ActiveLogView>();
                cfg.CreateMap<Survey, Areas.Admin.ViewModels.Survey.SurveyView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Survey.SurveyView, Survey>();
                #endregion

                #region 前台
                cfg.CreateMap<ViewModels.Apply.ApplyView, Apply>();
                cfg.CreateMap<ViewModels.Survey.SurveyView, Survey>();
                cfg.CreateMap<ViewModels.Message.MessageView, Message>();
                cfg.CreateMap<Message, ViewModels.Message.MessageView>();
                cfg.CreateMap<RelMessageApply, ViewModels.Message.MessageView>();
                #endregion
            });
        }
    }
}