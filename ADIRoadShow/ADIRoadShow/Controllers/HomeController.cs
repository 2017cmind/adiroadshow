﻿using ADIRoadShow.Models;
using ADIRoadShow.Repositories;
using ADIRoadShow.ViewModels.Apply;
using ADIRoadShow.ActionFilters;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ADIRoadShow.Utility.Cmind;

namespace ADIRoadShow.Controllers
{
    [ErrorHandleActionFilter]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            //CookieHelper.Del("RetrunUrl");

            return View();
        }

        public ActionResult Test()
        {
            //CookieHelper.Del("RetrunUrl");

            return View();
        }
    }
}