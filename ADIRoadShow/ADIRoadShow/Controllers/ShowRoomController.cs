﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ADIRoadShow.ActionFilters;
using ADIRoadShow.Models;
using ADIRoadShow.Repositories;
using ADIRoadShow.Utility.Cmind;
using ADIRoadShow.ViewModels.Message;
using AutoMapper;

namespace ADIRoadShow.Controllers
{
    [MemberAuthorize]
    [ErrorHandleActionFilter]
    public class ShowRoomController : BaseController
    {
        private MessageRepository messageRepository = new MessageRepository();
        private ApplyRepository applyRepository = new ApplyRepository();
        private ActiveLogRepository activeLogRepository = new ActiveLogRepository();
        // GET: ShowRoom
        public ActionResult VR()
        {
            var model = new MessageView();
            model.ShowRoomType = ShowRoomType.VR;
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (!memberInfo.IsAdmin) {
                activeLogRepository.Record(memberInfo.ID, (int)ShowRoomType.VR);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VR(MessageView model)
        {
            if (ModelState.IsValid) {

                Message data = Mapper.Map<Message>(model);
                var memberInfo = MemberInfoHelper.GetMemberInfo();
                data.ApplyId = memberInfo.ID;
                data.ShowRoomType = (int)ShowRoomType.VR;
                data.IsAdmin = memberInfo.IsAdmin;
                messageRepository.Insert(data);
                ShowMessage(true, "留言成功");
            }
            else
            {
                ShowMessage(true, "留言失敗");
            }
            return RedirectToAction(nameof(ShowRoomController.VR));
        }

        public ActionResult Video(int exhibitionType = 0)
        {
            var model = new MessageView();
            model.ShowRoomType = ShowRoomType.Video;
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (!memberInfo.IsAdmin)
            {
                activeLogRepository.Record(memberInfo.ID, (int)ShowRoomType.Video, exhibitionType);
            }
            return View(model);
        }

        public ActionResult VideoTest()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Video(MessageView model)
        {
            if (ModelState.IsValid)
            {

                Message data = Mapper.Map<Message>(model);
                var memberInfo = MemberInfoHelper.GetMemberInfo();
                data.ApplyId = memberInfo.ID;
                data.ShowRoomType = (int)ShowRoomType.Video;
                data.ExhibitionType = (int)ExhibitionCategory.Exhibition1;
                data.IsAdmin = memberInfo.IsAdmin;
                messageRepository.Insert(data);
                ShowMessage(true, "留言成功");
            }
            else
            {
                ShowMessage(true, "留言失敗");
            }
            return RedirectToAction(nameof(ShowRoomController.Video));
        }

        public JsonResult Refresh(int showRoomType, int exhibitionType = 0)
        {
            var member = MemberInfoHelper.GetMemberInfo();
            var result = "";
            var query = messageRepository.List((ShowRoomType)showRoomType, (ExhibitionCategory)exhibitionType).ToList();

            foreach (var item in query.Where(p => p.MessageId == 0))
            {
                var itemnickname = item.IsAdmin ? "管理員" : applyRepository.GetById(item.ApplyId).NickName;
                var itemislock = item.IsAdmin ? false: applyRepository.GetById(item.ApplyId).IsLock;
                if (!itemislock) { 
                
               
                var deleteLink = (item.ApplyId == member.ID && item.IsAdmin == member.IsAdmin) || member.IsAdmin ? "<a rel='nofollow' data-id='" + item.ID + "' onclick='reply.delete(this)'>刪除</a>" : "";
                result += "<li class='comment byuser comment-author-admin bypostauthor even thread-even depth-1' id='li-comment-3'>";
                result += "<div id='comment-3'><div class='comment-content'><div class='metadata'>";
                result += "<cite class='fn'><i class='fa fa-user'></i>" + itemnickname + "</cite> <br>";
                result += "<a>" + item.CreateTime.ToString("yyyy/MM/dd") + "</a></div>";
                result += "<p>" + item.Content + "</p><div class='reply'><a  rel='nofollow' onclick='reply.addNew(this)' data-messageid='" + item.ID + "'>回覆</a>" + deleteLink;
                result += "</div></div></div>";

                var replylist = query.Where(p => p.MessageId  == item.ID).ToList();
                foreach (var reply in replylist)
                {
                    var replynickname = reply.IsAdmin ? "管理員" : applyRepository.GetById(reply.ApplyId).NickName;
                    var replyislock = reply.IsAdmin ? false : applyRepository.GetById(reply.ApplyId).IsLock;
                    deleteLink = (reply.ApplyId == member.ID && reply.IsAdmin == member.IsAdmin) || member.IsAdmin ? "<a rel='nofollow' data-id='" + reply.ID + "' onclick='reply.delete(this)'>刪除</a>" : "";
                    if (!replyislock)
                    {
                        result += "<ol class='children'><li class='comment byuser comment-author-admin bypostauthor odd alt depth-2' id='li-comment-5'><div id='comment-5'><div class='comment-content'><div class='metadata'>";
                        result += "<cite class='fn'><i class='fa fa-user'></i>" + replynickname + "</cite> <br>";
                        result += "<a>" + reply.CreateTime.ToString("yyyy/MM/dd") + "</a></div>";
                        result += "<p>" + reply.Content + "</p><div class='reply'><a  rel='nofollow' onclick='reply.addNew(this)' data-messageid='" + reply.MessageId + "'>回覆</a>" + deleteLink + " </div></div></div></li></ol>";
                    }
                }
                result += "</li>";
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VideoReply(string message, string messageId, int exhibitionCategory = 0)
        {
            var result = "";
            //Message data = Mapper.Map<Message>(model);
            Message data = new Message();
            data.MessageId = Convert.ToInt32(messageId);
            data.Content = message;
           
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            data.ApplyId = memberInfo.ID;
            data.ShowRoomType = (int)ShowRoomType.Video;
            data.ExhibitionType = exhibitionCategory;
            data.IsAdmin = memberInfo.IsAdmin;
            messageRepository.Insert(data);
            return Json(result,JsonRequestBehavior.AllowGet);
        }
        public JsonResult VRReply(string message, string messageId)
        {
            var result = "";
            //Message data = Mapper.Map<Message>(model);
            Message data = new Message();
            data.MessageId = Convert.ToInt32(messageId);
            data.Content = message;

            var memberInfo = MemberInfoHelper.GetMemberInfo();
            data.ApplyId = memberInfo.ID;
            data.ShowRoomType = (int)ShowRoomType.VR;
            data.ExhibitionType = 0;
            data.IsAdmin = memberInfo.IsAdmin;
            messageRepository.Insert(data);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int id)
        {
            var result = "success";
            messageRepository.Delete(id);
            //ShowMessage(true, "刪除成功");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Quit(int showRoomType,int exhibitionType = 0)
        {
            var result = "success";
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (!memberInfo.IsAdmin)
            {
                activeLogRepository.UpdateEndTime(memberInfo.ID, showRoomType , exhibitionType);
            }
            CookieHelper.Del("RetrunUrl");

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
    }
}