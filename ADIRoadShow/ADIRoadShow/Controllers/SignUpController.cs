﻿using ADIRoadShow.ActionFilters;
using ADIRoadShow.Models;
using ADIRoadShow.Models.MailTemplate;
using ADIRoadShow.Repositories;
using ADIRoadShow.Utility.Cmind;
using ADIRoadShow.ViewModels.Apply;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ADIRoadShow.Controllers
{
    [ErrorHandleActionFilter]
    public class SignUpController : BaseController
    {
        private ApplyRepository applyRepository = new ApplyRepository();
        // GET: SignUp
        public ActionResult Index()
        {
            var model = new ApplyView();
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ApplyView model)
        {
            if (model.groups == null) {

                ModelState.AddModelError("groups", "請至少選取一項");
            }
            var query = applyRepository.Query(null, model.Email, null, null);
            if (query.Any()) {
                ShowMessage(false, "此信箱已註冊過");
            }
            if (ModelState.IsValid && !query.Any())
            {
                var dealercookie = CookieHelper.Get("Dealer");
                model.Category = model.Category == "其他" && !string.IsNullOrEmpty(model.CategoryTxt) ? "其他-" + model.CategoryTxt : model.Category;
                model.EventInformation = String.Join(",", model.groups);
                Apply data = Mapper.Map<Apply>(model);
                if (dealercookie != null)
                {
                    data.DealerId = EnumHelper.GetEnumFromDescription(dealercookie, typeof(Dealer));
                }
                applyRepository.Insert(data);
                model.Url = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Host;
                var message = new MailSuccessTemplate(model);
                MailHelper.SendEmail(message);
                Session["SignUp"] = "Success";
                return RedirectToAction(nameof(SignUpController.Success));
            }

            return View(model);
        }
        public ActionResult Success()
        {
            if (Session["SignUp"] == null)
            {
                RedirectToAction(nameof(SignUpController.Index));
            }
            else { 
            }
            Session["SignUp"] = null;
            return View();
        }

        public ActionResult Disclaimer()
        {
            return View();
        }
    }
}