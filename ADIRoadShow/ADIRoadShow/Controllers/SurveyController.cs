﻿using ADIRoadShow.ViewModels.Survey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ADIRoadShow.Repositories;
using ADIRoadShow.Utility.Cmind;
using ADIRoadShow.Models;
using AutoMapper;

namespace ADIRoadShow.Controllers
{
    public class SurveyController : BaseController
    {
        private SurveyRepository surveyRepository = new SurveyRepository();
        // GET: Survey
        public ActionResult Index()
        {
            var model = new SurveyView();
            var member = MemberInfoHelper.GetMemberInfo();
            if (member != null)
            {
                model.DealerId = member.DealerId.HasValue ? member.DealerId : Dealer.None;
            }

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(SurveyView model)
        {
            if (model.groups == null)
            {
                ModelState.AddModelError("groups", "請至少選取一項");
            }
            if (ModelState.IsValid)
            {
                model.Information = model.Information == "其他" && !string.IsNullOrEmpty(model.InformationTxt) ? "其他-" + model.InformationTxt : model.Information;
                model.Project = model.Project == "其他" && !string.IsNullOrEmpty(model.ProjectTxt) ? "其他-" + model.ProjectTxt : model.Project;
                model.Interest = String.Join(",", model.groups);
                Survey data = Mapper.Map<Survey>(model);
                surveyRepository.Insert(data);
                ShowMessage(true, "填寫成功，感謝您填寫此問卷");
                return RedirectToAction(nameof(SurveyController.Index));
            }

            return View(model);
        }
    }
}