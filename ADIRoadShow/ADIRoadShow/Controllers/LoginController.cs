﻿using ADIRoadShow.ActionFilters;
using ADIRoadShow.Models;
using ADIRoadShow.Models.Cmind;
using ADIRoadShow.Repositories;
using ADIRoadShow.Utility.Cmind;
using ADIRoadShow.ViewModels.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ADIRoadShow.Controllers
{
    //[MemberAuthorize]
    [ErrorHandleActionFilter]
    public class LoginController : BaseController
    {
        private ApplyRepository applyRepository = new ApplyRepository();
        private AdminRepository adminRepository = new AdminRepository();
        private ActiveLogRepository activeLogRepository = new ActiveLogRepository();

        // GET: Login
        public ActionResult Index()
        {
            bool isLogin = MemberInfoHelper.IsLogin();
            if (!isLogin) {
                RedirectToAction("Index", "Home");
            }
            var login = new LoginView();
            return View(login);
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginView login)
        {
            if (ModelState.IsValid)
            {
                var member = applyRepository.Login(login.Account, login.Password);
                var admin = adminRepository.Login(login.Account, login.Password);
                //var admininfo = AdminInfoHelper.GetAdminInfo();
                if (member != null || admin != null)
                {
                    if (member != null)
                    {
                        MemberInfo memberInfo = new MemberInfo();
                        memberInfo.ID = member.ID;
                        memberInfo.IsAdmin = false;
                        memberInfo.DealerId = (Dealer)member.DealerId;
                        CookieHelper.SetCookie("Dealer", memberInfo.DealerId);
                        MemberInfoHelper.Login(memberInfo, true);
                    }
                    if (admin != null)
                    {
                        MemberInfo memberInfo = new MemberInfo();
                        memberInfo.ID = admin.ID;
                        memberInfo.IsAdmin = true;
                        MemberInfoHelper.Login(memberInfo, true);
                    }
                    ShowMessage(true, "登入成功");
                    var url = CookieHelper.Get("RetrunUrl");
                    if (url != null && (url.IndexOf("Video") != -1 || url.IndexOf("VR") != -1))
                    {
                        return Redirect(url); 
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }

                }
                else
                {
                    ModelState.AddModelError("Password", "帳號或密碼錯誤");
                }
            }
            return View(login);
        }

        public ActionResult Logout(int showRoomType, int exhibitionType = 0)
        {
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (!memberInfo.IsAdmin)
            {
                activeLogRepository.UpdateEndTime(memberInfo.ID, showRoomType, exhibitionType);
            }
            MemberInfoHelper.Logout();
            return RedirectToAction("Index","Home");
        }
    }
}