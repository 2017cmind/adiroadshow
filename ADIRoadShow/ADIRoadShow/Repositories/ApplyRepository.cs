﻿using ADIRoadShow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Repositories
{
    public class ApplyRepository
    {
        private ADIRoadShowDBEntities db = new ADIRoadShowDBEntities();

        public Apply Login(string account, string password)
        {
            var member = db.Apply.Where(p => p.Email == account && p.Phone == password).FirstOrDefault();
            return member;
        }

        public IQueryable<Apply> Query(string name, string email, Dealer ? dealer, string phone)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(phone))
            {
                query = query.Where(p => p.Phone == phone);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.LastName.Contains(name) || p.FirstName.Contains(name));
            }
            if (!string.IsNullOrEmpty(email))
            {
                query = query.Where(p => p.Email == email);
            }
            if (dealer.HasValue)
            {
                query = query.Where(p => p.DealerId == (int)dealer);
            }
            return query;
        }

        public void IsLock(int id)
        {
            Apply data = GetById(id);
            data.IsLock = data.IsLock ? false : true;
            db.SaveChanges();
        }

        public IQueryable<Apply> GetAll()
        {
            var query = db.Apply;
            return query;
        }

        public int Insert(Apply newData)
        {
            DateTime now = DateTime.Now;
            newData.CreateTime = now;
            db.Apply.Add(newData);
            db.SaveChanges();
            return newData.ID;
        }

        public void Update(Apply data)
        {
            DateTime now = DateTime.Now;
            Apply update = db.Apply.Find(data.ID);
            update.FirstName = data.FirstName;
            update.DealerId = data.DealerId;
            update.LastName = data.LastName;
            update.NickName = data.NickName;
            update.Phone = data.Phone;
            update.IsLock = data.IsLock;
            update.Position = data.Position;
            update.Company = data.Company;
            update.Department = data.Department;
            update.Category = data.Category;
            update.EventInformation = data.EventInformation;
            update.JobCategory = data.JobCategory;
            update.Email = data.Email;
            db.SaveChanges();
        }

        public void isLock(Apply data)
        {
            DateTime now = DateTime.Now;
            Apply update = db.Apply.Find(data.ID);
            update.IsLock = data.IsLock;
            db.SaveChanges();
        }
        //public void Confirm(int id)
        //{
        //    DateTime now = DateTime.Now;
        //    Apply update = db.Apply.Find(id);
        //    update.Status = true ;
        //    db.SaveChanges();
        //}

        public void Delete(int id)
        {
            Apply delete = db.Apply.Find(id);
            db.Apply.Remove(delete);
            db.SaveChanges();
        }

        public Apply GetById(int id)
        {
            return db.Apply.Find(id);
        }
    }
}