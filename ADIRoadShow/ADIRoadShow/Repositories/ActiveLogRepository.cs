﻿using ADIRoadShow.Models;
using ADIRoadShow.Models.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Repositories
{
    public class ActiveLogRepository
    {
        private ADIRoadShowDBEntities db = new ADIRoadShowDBEntities();

        public void Insert(ActiveLog newData)
        {
            DateTime now = DateTime.Now;
            newData.OnlineTime = now;
            db.ActiveLog.Add(newData);
            db.SaveChanges();
        }

        public IQueryable<RelActiveLogApply> Query(ShowRoomType ?showRoomType, Dealer? dealer, ExhibitionCategory? exhibitionCategory)
        {
            var query = GetAll();
            if (showRoomType.HasValue)
            {
                query = query.Where(p => p.ShowRoomType == showRoomType);
            }
            if (dealer.HasValue)
            {
                query = query.Where(p => p.DealerId == dealer);
            }
            if (exhibitionCategory.HasValue)
            {
                query = query.Where(p => p.ExhibitionType == exhibitionCategory);
            }
            return query;
        }

        public IQueryable<RelActiveLogApply> GetAll()
        {
            var query = from n in db.ActiveLog
                        join t in db.Apply on n.ApplyId equals t.ID
                        select new RelActiveLogApply()
                        {
                            ID = n.ID,
                            DealerId = (Dealer)t.DealerId,
                            EndTime = n.EndTime ,
                            OnlineTime = n.OnlineTime,
                            Name = t.LastName + t.FirstName,
                            ShowRoomType = (ShowRoomType)n.ShowRoomType,
                            ExhibitionType = n.ExhibitionType != 0 ? (ExhibitionCategory)n.ExhibitionType : 0,
                            ApplyId = t.ID,
                            Company = t.Company,
                            Email = t.Email,
                            Phone = t.Phone
                        };
            return query;
        }

        public void Record(int applyId, int showRoomType, int exhibitionCategory = 0)
        {
            var query = db.ActiveLog.Where(p => p.ApplyId == applyId);
            if (query.Any()) {
                if (showRoomType != 0)
                {
                    query = query.Where(p => p.ShowRoomType == showRoomType);
                }
                
                if (exhibitionCategory != 0)
                {
                    query = query.Where(p => p.ExhibitionType == exhibitionCategory);
                }
                if (!query.Any())
                {
                    ActiveLog data = new ActiveLog();
                    DateTime now = DateTime.Now;
                    data.OnlineTime = now;
                    data.ShowRoomType = showRoomType;
                    data.ExhibitionType = exhibitionCategory;
                    data.ApplyId = applyId;
                    db.ActiveLog.Add(data);
                    db.SaveChanges();
                }
            }
            else
            {
                ActiveLog data = new ActiveLog();
                DateTime now = DateTime.Now;
                data.OnlineTime = now;
                data.ShowRoomType = showRoomType;
                data.ExhibitionType = exhibitionCategory;
                data.ApplyId = applyId;
                db.ActiveLog.Add(data);
                db.SaveChanges();
            }
        }

        public void UpdateEndTime(int applyId, int showRoomType, int exhibitionCategory = 0)
        {
            var query = db.ActiveLog.Where(p => p.ApplyId == applyId && p.ExhibitionType == exhibitionCategory && p.ShowRoomType == showRoomType);

            if (query.Any())
            {
                DateTime now = DateTime.Now;
                ActiveLog data = db.ActiveLog.Find(query.First().ID);
                data.EndTime = now;
                db.SaveChanges();
            }
        }
    }
}