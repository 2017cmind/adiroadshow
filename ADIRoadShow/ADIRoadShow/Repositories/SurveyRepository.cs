﻿using ADIRoadShow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Repositories
{
    public class SurveyRepository
    {
        private ADIRoadShowDBEntities db = new ADIRoadShowDBEntities();

        public IQueryable<Survey> Query(string project, string name,string information)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(project))
            {
                query = query.Where(p => p.Project.Contains(project));
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.Name.Contains(name));
            }
            if (!string.IsNullOrEmpty(information))
            {
                query = query.Where(p => p.Information.Contains(information));
            }
            return query;
        }

        public IQueryable<Survey> GetAll()
        {
            var query = db.Survey;
            return query;
        }

        public int Insert(Survey newData)
        {
            DateTime now = DateTime.Now;
            newData.CreateTime = now;
            db.Survey.Add(newData);
            db.SaveChanges();
            return newData.ID;
        }

        public Survey GetById(int id)
        {
            return db.Survey.Find(id);
        }
    }
}