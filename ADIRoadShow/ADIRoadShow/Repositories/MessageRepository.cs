﻿using ADIRoadShow.Models;
using ADIRoadShow.Models.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Repositories
{
    public class MessageRepository
    {
        private ADIRoadShowDBEntities db = new ADIRoadShowDBEntities();

        public IQueryable<RelMessageApply> Query(ShowRoomType showRoomType, bool ? islock, ExhibitionCategory ? exhibitionCategory)
        {
            var query = GetAll();
            if (showRoomType != 0)
            {
                query = query.Where(p => p.ShowRoomType == showRoomType);
            }
            if (islock.HasValue) {
                query = query.Where(p => p.IsLock == islock);
            }
            if (exhibitionCategory.HasValue && exhibitionCategory != ExhibitionCategory.None)
            {
                query = query.Where(p => p.ExhibitionType == exhibitionCategory);
            }
            return query;
        }

        public IQueryable<RelMessageApply> GetAll()
        {
            var query = from n in db.Message
                        join t in db.Apply on n.ApplyId equals t.ID
                        select new RelMessageApply()
                        {
                            ID = n.ID,
                            NickName = t.NickName,
                            CreateTime = n.CreateTime,
                            IsLock = n.IsAdmin ? false : t.IsLock,
                            IsAdmin = n.IsAdmin,
                            Name = t.LastName + t.FirstName,
                            ShowRoomType = (ShowRoomType)n.ShowRoomType,
                            ExhibitionType = (ExhibitionCategory)n.ExhibitionType,
                            ApplyId = t.ID,
                            MessageId = n.MessageId,
                            Content = n.Content
                        };
            return query;
        }

        public IQueryable<MessageApply> GetList()
        {
            var query = from n in db.Message
                        join t in db.Apply on n.ApplyId equals t.ID
                        select new MessageApply()
                        {
                            ID = n.ID,
                            NickName = n.IsAdmin ? "管理員":t.NickName,
                            CreateTime = n.CreateTime,
                            IsLock = n.IsAdmin ? false : t.IsLock,
                            IsAdmin = n.IsAdmin,
                            Name = t.LastName + t.FirstName,
                            ShowRoomType = n.ShowRoomType,
                            ExhibitionType = n.ExhibitionType,
                            ApplyId = t.ID,
                            MessageId = n.MessageId,
                            Content = n.Content
                        };
            return query;
        }

        public Message GetById(int id)
        {
            return db.Message.Find(id);
        }

        public void Delete(int id)
        {
            Message delete = db.Message.Find(id);
            var reply = db.Message.Where(p => p.MessageId == id);
            if (reply.Any()) {
                db.Message.RemoveRange(reply);
            }
            db.Message.Remove(delete);
            db.SaveChanges();
        }

        public int Insert(Message newData)
        {
            DateTime now = DateTime.Now;
            newData.CreateTime = now;
            db.Message.Add(newData);
            db.SaveChanges();
            return newData.ID;
        }

        public IQueryable<Message> List(ShowRoomType showRoomType, ExhibitionCategory exhibitionType)
        {
            var query = db.Message.Where(p => p.ShowRoomType == (int)showRoomType && p.ExhibitionType == (int)exhibitionType).OrderByDescending(p => p.CreateTime);
            return query;
        }
    }
}