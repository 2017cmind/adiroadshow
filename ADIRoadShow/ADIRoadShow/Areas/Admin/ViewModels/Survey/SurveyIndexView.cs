﻿using ADIRoadShow.Models;
using ADIRoadShow.Models.Cmind;
using ADIRoadShow.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADIRoadShow.Areas.Admin.ViewModels.Survey
{
    public class SurveyIndexView : PageQuery
    {
        public SurveyIndexView()
        {
            this.Sorting = "CreateTime";
            this.IsDescending = true;
        }
        public PageResult<SurveyView> PageResult { get; set; }

        public string SurveyDownloadToken { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "專案項目分類")]
        public string Project { get; set; }

        [Display(Name = "希望獲得訊息或服務")]
        public string Information { get; set; }

        [Display(Name = "是否更瞭解ADI應用解決方案")]
        public bool IsKnowing { get; set; }

        public List<SelectListItem> ProjectOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ProjectType>();
                result.Insert(0, new SelectListItem { Text = "全部", Value = "" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = EnumHelper.GetDescription(v),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> InformationOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<InformationType>();
                result.Insert(0, new SelectListItem { Text = "全部", Value = "" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = EnumHelper.GetDescription(v),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        [Display(Name = "建立日期")]
        public DateTime CreateTime { get; set; }
    }
}