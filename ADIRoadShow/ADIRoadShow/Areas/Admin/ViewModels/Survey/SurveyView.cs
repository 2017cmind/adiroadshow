﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Areas.Admin.ViewModels.Survey
{
    public class SurveyView
    {
        public int ID { get; set; }
        public int ShowRoomType { get; set; }
        [Display(Name = "姓名")]
        public string Name { get; set; }
        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Display(Name = "公司")]
        public string Company { get; set; }

        [Display(Name = "信箱")]
        public string Email { get; set; }

        [Display(Name = "是否了解ADI應用解決方案")]
        public bool IsKnowing { get; set; }

        [Display(Name = "感興趣主題")]
        public string Interest { get; set; }

        [Display(Name = "專案類型")]
        public string Project { get; set; }

        [Display(Name = "希望獲得訊息或服務")]
        public string Information { get; set; }

        [Display(Name = "建立日期")]
        public System.DateTime CreateTime { get; set; }
    }
}