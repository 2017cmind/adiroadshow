﻿using ADIRoadShow.Models;
using ADIRoadShow.Models.Cmind;
using ADIRoadShow.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADIRoadShow.Areas.Admin.ViewModels.ActiveLog
{
    public class ActiveLogIndexView : PageQuery
    {
        public ActiveLogIndexView()
        {
            this.Sorting = "OnlineTime";
            this.IsDescending = true;
        }
        public PageResult<ActiveLogView> PageResult { get; set; }

        [Display(Name = "經銷來源")]
        public Dealer? DealerId { get; set; }

        [Display(Name = "活動類型")]
        public ShowRoomType? ShowRoomType { get; set; }

        [Display(Name = "活動分類")]
        public ExhibitionCategory? ExhibitionType { get; set; }

        public List<SelectListItem> ExhibitionCategoryOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ExhibitionCategory>();
                result.Insert(0, new SelectListItem { Text = "全部", Value = "" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> DealerCategoryOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Dealer>();
                result.Insert(0, new SelectListItem { Text = "全部", Value = "" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> ShowRoomTypeOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ShowRoomType>();
                result.Insert(0, new SelectListItem { Text = "全部", Value = "" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}