﻿using ADIRoadShow.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Areas.Admin.ViewModels.ActiveLog
{
    public class ActiveLogView
    {
        public int ID { get; set; }
        [Display(Name = "活動報名ID")]
        public int ApplyId { get; set; }
        [Display(Name = "活動類型")]
        public ShowRoomType ShowRoomType { get; set; }
        [Display(Name = "姓名")]
        public string Name { get; set; }
        public string Email { get; set; }
        [Display(Name = "電話")]
        public string Phone { get; set; }
        [Display(Name = "公司")]
        public string Company { get; set; }
        [Display(Name = "活動分類")]
        public ExhibitionCategory ExhibitionType { get; set; }
        [Display(Name = "經銷來源")]
        public Dealer? DealerId { get; set; }
        [Display(Name = "開始時間")]
        public DateTime OnlineTime { get; set; }
        [Display(Name = "結束時間")]
        public DateTime EndTime { get; set; }
    }
}