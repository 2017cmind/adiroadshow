﻿using ADIRoadShow.Models;
using ADIRoadShow.Models.Cmind;
using ADIRoadShow.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADIRoadShow.Areas.Admin.ViewModels.Apply
{
    public class ApplyIndexView : PageQuery
    {
        public ApplyIndexView(){
            this.Sorting = "CreateTime";
            this.IsDescending = true;
        }
        public PageResult<ApplyView> PageResult { get; set; }

        [Display(Name = "經銷來源")]
        public Dealer ? DealerId { get; set; }

        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "名")]
        public string FirstName { get; set; }

        [Display(Name = "姓")]
        public string LastName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        public string DownloadToken { get; set; }

        [Display(Name = "職業分類")]
        public string JobCategory { get; set; }

        public List<SelectListItem> DealerCategoryOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Dealer>();
                result.Insert(0, new SelectListItem { Text = "全部", Value = "" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}