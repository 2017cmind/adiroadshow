﻿using ADIRoadShow.Models;
using ADIRoadShow.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADIRoadShow.Areas.Admin.ViewModels.Apply
{
    public class Group
    {
        public string Name { get; set; }
    }
    public class ApplyView
    {
        public ApplyView()
        {
            //And some data to play with
            var allGroups = new List<Group>();
            var values = EnumHelper.GetValues<EventInformationType>();

            foreach (var v in values)
            {
                allGroups.Add(new Group
                {
                    Name = EnumHelper.GetDescription(v),
                });
            }
            this.CheckOptions = allGroups;
        }
        public List<Group> CheckOptions { get; set; }

        /// <summary>
        /// check list
        /// </summary>
        public List<string> groups { get; set; }

        public int ID { get; set; }

        [Display(Name = "經銷來源")]
        public Dealer DealerId { get; set; }

        [Required]
        [Display(Name = "名")]
        [StringLength(20, ErrorMessage = "{0}不能超過{1}個字")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "{0}不能超過{1}個字")]
        [Display(Name = "姓")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "暱稱")]
        [StringLength(40, ErrorMessage = "{0}不能超過{1}個字")]
        public string NickName { get; set; }     

        [Required]
        [StringLength(60, ErrorMessage = "{0}不能超過{1}個字")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "{0}不能超過{1}個字")]
        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "產業分類")]
        public string Category { get; set; }

        [StringLength(20, ErrorMessage = "{0}不能超過{1}個字")]
        [Display(Name = "產業分類")]
        public string CategoryTxt { get; set; }

        [Required]
        [Display(Name = "職業分類")]
        public string JobCategory { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0}不能超過{1}個字")]
        [Display(Name = "公司")]
        public string Company { get; set; }

        [StringLength(100, ErrorMessage = "{0}不能超過{1}個字")]
        [Display(Name = "職位")]
        public string Position { get; set; }

        [Display(Name = "部門")]
        [StringLength(100, ErrorMessage = "{0}不能超過{1}個字")]
        public string Department { get; set; }

        [Display(Name = "取得活動資訊管道")]
        public string EventInformation { get; set; }

        [Display(Name = "封鎖狀態")]
        public bool IsLock { get; set; }

        [Display(Name = "建立時間")]
        public System.DateTime CreateTime { get; set; }
        public List<SelectListItem> JobCategoryOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<JobCategory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = EnumHelper.GetDescription(v),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> DealerCategoryOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Dealer>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}