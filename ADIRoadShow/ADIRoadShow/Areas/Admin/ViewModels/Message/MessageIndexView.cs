﻿using ADIRoadShow.Models;
using ADIRoadShow.Models.Cmind;
using ADIRoadShow.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADIRoadShow.Areas.Admin.ViewModels.Message
{
    public class MessageIndexView : PageQuery
    {
        public MessageIndexView()
        {
            this.Sorting = "CreateTime";
            this.IsDescending = true;
        }
        public PageResult<MessageView> PageResult { get; set; }

        [Display(Name = "暱稱")]
        public string NickName { get; set; }

        [Display(Name = "展覽分類")]
        public ShowRoomType ShowRoomType { get; set; }

        [Display(Name = "活動類別")]
        public ExhibitionCategory ? ExhibitionType { get; set; }

        public List<SelectListItem> ShowRoomTypeOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ShowRoomType>();
                result.Insert(0, new SelectListItem { Text = "全部", Value = "" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> ExhibitionTypeOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ExhibitionCategory>();
                result.Insert(0, new SelectListItem { Text = "全部", Value = "" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        [Display(Name = "封鎖")]
        public bool ? IsLock { get; set; }

        [Display(Name = "留言內容")]
        public string Content { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateTime { get; set; }
    }
}