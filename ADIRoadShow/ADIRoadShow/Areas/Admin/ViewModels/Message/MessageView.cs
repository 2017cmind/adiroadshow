﻿using ADIRoadShow.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ADIRoadShow.Areas.Admin.ViewModels.Message
{
    public class MessageView
    {
        public int ID { get; set; }

        [Display(Name = "展覽分類")]
        public ShowRoomType ShowRoomType { get; set; }
        public int ApplyId { get; set; }
        [Display(Name = "暱稱")]
        public string NickName { get; set; }

        [Display(Name = "活動類別")] 
        public ExhibitionCategory ExhibitionType { get; set; }

        public int MessageId { get; set; }
        public bool IsAdmin { get; set; }
        [Display(Name = "封鎖")]
        public bool IsLock { get; set; }

        [Display(Name = "留言內容")]
        public string Content { get; set; }
        public System.DateTime CreateTime { get; set; }
    }
}