﻿using ADIRoadShow.ActionFilters;
using ADIRoadShow.Areas.Admin.ViewModels.Apply;
using ADIRoadShow.Models;
using ADIRoadShow.Models.Cmind;
using ADIRoadShow.Models.MailTemplate;
using ADIRoadShow.Repositories;
using ADIRoadShow.Utility.Cmind;
using AutoMapper;
using ClosedXML.Excel;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ADIRoadShow.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ApplyAdminController : BaseAdminController
    {
        // GET: Admin/ApplyAdmin
        private ApplyRepository applyRepository = new ApplyRepository();
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public ActionResult Index(ApplyIndexView model, string method = "")
        {
            var query = applyRepository.Query(model.Name, model.Email, model.DealerId, model.Phone);
            var pageResult = query.ToPageResult<Apply>(model);
            model.PageResult = Mapper.Map<PageResult<ApplyView>>(pageResult);
            if (method == "excel")
            {
                string fileName = string.Format("{0}_{1}.xlsx", "報名資料", DateTime.Now.ToString("yyyyMMddHHmmss"));
                byte[] byteArray = generateExcelByteArray(query, fileName);

                CookieHelper.SetCookie(nameof(model.DownloadToken), model.DownloadToken);

                return File(byteArray, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            ApplyView model;

            var query = applyRepository.GetById(id);
            model = Mapper.Map<ApplyView>(query);
            model.CategoryTxt = model.Category.IndexOf("其他") != -1 ? model.Category.Replace("其他-", "") : null;

            model.Category = model.Category.IndexOf("其他") != -1 ? "其他" : model.Category;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ApplyView model)
        {
            if (model.groups == null)
            {

                ModelState.AddModelError("groups", "請至少選取一項");
            }
            if (ModelState.IsValid)
            {
                model.Category = model.Category == "其他" ? "其他-" + model.CategoryTxt : model.Category;
                model.EventInformation = String.Join(",", model.groups);
                Apply data = Mapper.Map<Apply>(model);
                applyRepository.Update(data);
                ShowMessage(true, "儲存成功");
                return RedirectToAction(nameof(ApplyAdminController.Edit), new { id = model.ID });
            }
            else
            {
                ShowMessage(false, "輸入資料有誤");
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            applyRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction(nameof(ApplyAdminController.Index));
        }

        public ActionResult Lock(int id)
        {
            applyRepository.IsLock(id);
            ShowMessage(true, "已修改會員狀態");
            return RedirectToAction(nameof(ApplyAdminController.Index));
        }

        #region Excel       
        /// <summary>
        /// 驗證是否可匯出Excel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxValidateAntiForgeryTokenAttribute]
        public JsonResult ValidateExcel(ApplyIndexView model)
        {
            try
            {
                var data = applyRepository.Query(model.Name, model.Email, model.DealerId, model.Phone);
                int count = data.Count();
                if (count == 0)
                {
                    string msg = "匯出的資料筆數為零";

                    return Json(new { result = false, errorMsg = msg });
                }

                return Json(new { result = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return Json(new { result = false, errorMsg = "Server Error" });
            }
        }
        /// <summary>
        /// 產生excel
        /// </summary>
        /// <param name = "source" ></ param >
        /// < param name="fileName"></param>
        /// <returns></returns>
        public byte[] generateExcelByteArray(IQueryable<Apply> source, string fileName)
        {
            //建立 excel 物件
            XLWorkbook workbook = new XLWorkbook();
            var sheet = workbook.Worksheets.Add("活動表單");

            int rowIdx = 1;
            sheet.Cell(rowIdx, 1).Value = "經銷商";
            sheet.Cell(rowIdx, 2).Value = "姓名";
            sheet.Cell(rowIdx, 3).Value = "暱稱";
            sheet.Cell(rowIdx, 4).Value = "電話";
            sheet.Cell(rowIdx, 5).Value = "Email";
            sheet.Cell(rowIdx, 6).Value = "產業別";
            sheet.Cell(rowIdx, 7).Value = "工作性質";
            sheet.Cell(rowIdx, 8).Value = "公司";
            sheet.Cell(rowIdx, 9).Value = "部門";
            sheet.Cell(rowIdx, 10).Value = "職稱";
            sheet.Cell(rowIdx, 11).Value = "活動資訊來源";
            sheet.Cell(rowIdx, 12).Value = "報名日期";
            rowIdx++;

            foreach (var item in source)
            {
                sheet.Cell(rowIdx, 1).Value = (Dealer)item.DealerId;
                sheet.Cell(rowIdx, 2).Value = item.LastName + item.FirstName;
                sheet.Cell(rowIdx, 3).Value = item.FirstName;
                sheet.Cell(rowIdx, 4).Value = string.Concat("'", item.Phone);
                sheet.Cell(rowIdx, 5).Value = item.Email;
                sheet.Cell(rowIdx, 6).Value = item.Category;
                sheet.Cell(rowIdx, 7).Value = item.JobCategory;
                sheet.Cell(rowIdx, 8).Value = item.Company;
                sheet.Cell(rowIdx, 9).Value = item.Department;
                sheet.Cell(rowIdx, 10).Value = item.Position;
                sheet.Cell(rowIdx, 11).Value = item.EventInformation;
                sheet.Cell(rowIdx, 12).Value = item.CreateTime;
                rowIdx++;
            }
            string absolutepath = Server.MapPath("~/ExcelFile");
            string filePath = Path.Combine(absolutepath, fileName);
            workbook.SaveAs(filePath);

            byte[] byteArray;

            using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                //Create and populate a memorystream with the contents of the
                MemoryStream mstream = new MemoryStream();
                fileStream.CopyTo(mstream);

                //Convert the memorystream to an array of bytes.
                byteArray = mstream.ToArray();

                //Clean up the memory stream
                mstream.Flush();
                mstream.Close();
            }

            // delete the file when it is been added to memory stream
            System.IO.File.Delete(filePath);

            return byteArray;
        }
        #endregion
    }
}