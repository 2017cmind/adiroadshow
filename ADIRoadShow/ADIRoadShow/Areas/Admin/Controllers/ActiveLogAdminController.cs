﻿using ADIRoadShow.ActionFilters;
using ADIRoadShow.Areas.Admin.ViewModels.ActiveLog;
using ADIRoadShow.Models.Cmind;
using ADIRoadShow.Models.JoinModels;
using ADIRoadShow.Repositories;
using ADIRoadShow.Utility.Cmind;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ADIRoadShow.Areas.Admin.Controllers
{
    public class ActiveLogAdminController : BaseAdminController
    {
        private ActiveLogRepository activeLogRepository = new ActiveLogRepository();

        [Authorize]
        [ErrorHandleAdminActionFilter]
        // GET: Admin/ActiveLogAdmin
        public ActionResult Index(ActiveLogIndexView model)
        {
            var query = activeLogRepository.Query(model.ShowRoomType, model.DealerId, model.ExhibitionType);
            var pageResult = query.ToPageResult<RelActiveLogApply>(model);
            model.PageResult = Mapper.Map<PageResult<ActiveLogView>>(pageResult);
            return View(model);
        }
    }
}