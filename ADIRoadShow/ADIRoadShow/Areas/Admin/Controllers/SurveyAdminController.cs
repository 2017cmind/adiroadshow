﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ADIRoadShow.ActionFilters;
using ADIRoadShow.Areas.Admin.ViewModels.Survey;
using ADIRoadShow.Models;
using ADIRoadShow.Models.Cmind;
using ADIRoadShow.Repositories;
using ADIRoadShow.Utility.Cmind;
using AutoMapper;
using ClosedXML.Excel;
using NLog;

namespace ADIRoadShow.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class SurveyAdminController : BaseAdminController
    {
        // GET: Admin/SurveyAdmin
        private SurveyRepository surveyRepository = new SurveyRepository();
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public ActionResult Index(SurveyIndexView model, string method = "")
        {
            var query = surveyRepository.Query(model.Project, model.Name, model.Information);
            var pageResult = query.ToPageResult<Survey>(model);
            model.PageResult = Mapper.Map<PageResult<SurveyView>>(pageResult);
            if (method == "excel")
            {
                string fileName = string.Format("{0}_{1}.xlsx", "問卷資料", DateTime.Now.ToString("yyyyMMddHHmmss"));
                byte[] byteArray = generateExcelByteArray(query, fileName);

                CookieHelper.SetCookie(nameof(model.SurveyDownloadToken), model.SurveyDownloadToken);

                return File(byteArray, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            SurveyView model;

            if (id != 0) {
                var query = surveyRepository.GetById(id);
                model = Mapper.Map<SurveyView>(query);
            }
            else {
                return RedirectToAction(nameof(SurveyAdminController.Error));
            }
            return View(model);
        }

        #region Excel       
        /// <summary>
        /// 驗證是否可匯出Excel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxValidateAntiForgeryTokenAttribute]
        public JsonResult ValidateExcel(SurveyIndexView model)
        {
            try
            {
                var data = surveyRepository.Query(model.Project, model.Name, model.Information);
                int count = data.Count();
                if (count == 0)
                {
                    string msg = "匯出的資料筆數為零";

                    return Json(new { result = false, errorMsg = msg });
                }

                return Json(new { result = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return Json(new { result = false, errorMsg = "Server Error" });
            }
        }
        /// <summary>
        /// 產生excel
        /// </summary>
        /// <param name = "source" ></ param >
        /// < param name="fileName"></param>
        /// <returns></returns>
        public byte[] generateExcelByteArray(IQueryable<Survey> source, string fileName)
        {
            //建立 excel 物件
            XLWorkbook workbook = new XLWorkbook();
            var sheet = workbook.Worksheets.Add("問卷表單");
            int rowIdx = 1;
            sheet.Cell(rowIdx, 1).Value = "姓名";
            sheet.Cell(rowIdx, 2).Value = "手機";
            sheet.Cell(rowIdx, 3).Value = "公司";
            sheet.Cell(rowIdx, 4).Value = "Email";
            sheet.Cell(rowIdx, 5).Value = "是否對ADI應用解決辦法了解";
            sheet.Cell(rowIdx, 6).Value = "感興趣的主題";
            sheet.Cell(rowIdx, 7).Value = "專案項目類型";
            sheet.Cell(rowIdx, 8).Value = "想獲得訊息或服務";
            sheet.Cell(rowIdx, 9).Value = "填寫日期";
            rowIdx++;

            foreach (var item in source)
            {
                sheet.Cell(rowIdx, 1).Value = item.Name;
                sheet.Cell(rowIdx, 2).Value = string.Concat("'", item.Phone);
                sheet.Cell(rowIdx, 3).Value = item.Company;
                sheet.Cell(rowIdx, 4).Value = item.Email;
                sheet.Cell(rowIdx, 5).Value = item.IsKnowing ? "是" :"否";
                sheet.Cell(rowIdx, 6).Value = item.Interest.Replace(",", "\n");
                sheet.Cell(rowIdx, 6).Style.Alignment.WrapText = true;
                sheet.Cell(rowIdx, 7).Value = item.Project;
                sheet.Cell(rowIdx, 8).Value = item.Information;
                sheet.Cell(rowIdx, 9).Value = item.CreateTime;
                rowIdx++;
            }
            sheet.Columns("F").AdjustToContents();

            string absolutepath = Server.MapPath("~/ExcelFile");
            string filePath = Path.Combine(absolutepath, fileName);
            workbook.SaveAs(filePath);

            byte[] byteArray;

            using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                //Create and populate a memorystream with the contents of the
                MemoryStream mstream = new MemoryStream();
                fileStream.CopyTo(mstream);

                //Convert the memorystream to an array of bytes.
                byteArray = mstream.ToArray();

                //Clean up the memory stream
                mstream.Flush();
                mstream.Close();
            }

            // delete the file when it is been added to memory stream
            System.IO.File.Delete(filePath);

            return byteArray;
        }
        #endregion
    }
}