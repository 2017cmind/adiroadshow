﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ADIRoadShow.ActionFilters;
using ADIRoadShow.Areas.Admin.ViewModels.Message;
using ADIRoadShow.Models;
using ADIRoadShow.Models.Cmind;
using ADIRoadShow.Models.JoinModels;
using ADIRoadShow.Repositories;
using ADIRoadShow.Utility.Cmind;
using AutoMapper;

namespace ADIRoadShow.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class MessageAdminController : BaseAdminController
    {
        private MessageRepository messageRepository = new MessageRepository();
        private ApplyRepository applyRepository = new ApplyRepository();

        // GET: Admin/MessageAdmin
        public ActionResult Index(MessageIndexView model)
        {
            var query = messageRepository.Query(model.ShowRoomType, model.IsLock, model.ExhibitionType);
            var pageResult = query.ToPageResult<RelMessageApply>(model);
            model.PageResult = Mapper.Map<PageResult<MessageView>>(pageResult);
            return View(model);
        }

        //public ActionResult Delete(int id)
        //{
        //    messageRepository.Delete(id);
        //    ShowMessage(true, "刪除成功");
        //    return RedirectToAction(nameof(MessageAdminController.Index));
        //}

        public ActionResult Lock(int id)
        {
            applyRepository.IsLock(id);
            ShowMessage(true, "已修改會員狀態");
            return RedirectToAction(nameof(MessageAdminController.Index));
        }
    }
}