﻿var reply = {
    addNew: function (e) {
        const $this = $(e);
        var value = $this.data("messageid");
        $('input[name="MessageId"]').val(value);
        var $target = $this.parent().parent().find(".metadata .fn");
        $('textarea[name=Content]').val("回覆" + $target.text() + ":");
        $("textarea[name=Content]").focus();
        window.location.hash = "";
        window.location.hash = "Content";

    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().parent().parent().parent().remove();
    },
    childreply: function (e) {
        $("#reply").click();
        $('#replyform').validate({
            rules: {
                Content: "required",
            },
            submitHandler: function (form) {
                $('#replyform input:submit').attr('disabled', 'disabled');

                form.submit();
            }
        });
    },
    delete: function (e) {
        //clearInterval(interval);
        const $this = $(e);
        var value = $this.data("id");
        $.ajax({
            type: 'GET',
            url: '/ShowRoom/Delete?id=' + value,
        }).done(function (result) {
            if (result) {
                doStuff($.urlParam("exhibitionType"));
            }
        }).fail(function () {
            console.log("錯誤");
        });
    }
};
/*var interval = setInterval(function () {*/
//    doStuff();
/*}, 1000);*/

function addPartialViewValid() {
    $("form").each(function () { $.data($(this)[0], 'validator', false); });
    $.validator.unobtrusive.parse("form");
}


