﻿var allCategory = {
    "exhibitionType": [
        {
            "name": "震動分析與資料擷取量測平台的設計經驗分享",     
            "dateTimeVideo":"2021-11-30T11:01:00",
            "url": "https://www.youtube.com/embed/ZDDq84NaF6U",
            "type": 1,
        },
        {
            "name": "水質監控系統",
            "dateTimeVideo": "2021-12-01T15:01:00",
            "url": "https://www.youtube.com/embed/wpZvfHpRjv8",
            "type": 2
        },
        {
            "name": "第3代無線狀態監控模組應用方案",
            "dateTimeVideo": "2021-12-02T10:00:00",
            "url": "https://www.youtube.com/embed/Ymc3dvhSqRE",
            "type": 3
        },
        {
            "name": "回音消除演算法應用方案",
            "dateTimeVideo": "2021-12-02T14:00:00",
            "url": "https://www.youtube.com/embed/1-NXb5SHOo8",
            "type": 4
        },
        {
            "name": "新一代高畫質多媒體介面 (HDMI)產品應用設計經驗分享",
            "dateTimeVideo": "2021-12-03T11:01:00",
            "url": "https://www.youtube.com/embed/lRYsDxcMYec",
            "type": 5
        },
        {
            "name": "新一代 3D深度感測器產品應用設計經驗分享",
            "dateTimeVideo": "2021-12-03T15:01:00",
            "url": "https://www.youtube.com/embed/57Zv8_islpQ",
            "type": 6
        }
    ]
};
