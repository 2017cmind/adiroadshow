﻿var reply = {
    addNew: function (e) {
        const $this = $(e);
        var value = $this.data("messageid");
        $('input[name="MessageId"]').val(value);
        var $target = $this.parent().parent().find(".metadata .fn");
        $('textarea[name=Content]').val("回覆" + $target.text() + ":");
        $("textarea[name=Content]").focus();
        window.location.hash = "";
        window.location.hash = "content";
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().parent().parent().parent().remove();
    },
    childreply: function (e) {
        $("#reply").click();
        $('#replyform').validate({
            rules: {
                Content: "required",
            },
            submitHandler: function (form) {
                $('#replyform input:submit').attr('disabled', 'disabled');

                form.submit();
            }
        });
    },
    delete: function (e) {
        //clearInterval(interval);
        const $this = $(e);
        var value = $this.data("id");
        $.ajax({
            type: 'GET',
            url: '/ShowRoom/Delete?id=' + value,
        }).done(function (result) {
            if (result) {
                doStuff();
            }
        }).fail(function () {
            console.log("錯誤");
        });
    }
};
//var interval = setInterval(function () {
    doStuff();
/*}, 2000);*/

function addPartialViewValid() {
    $("form").each(function () { $.data($(this)[0], 'validator', false); });
    $.validator.unobtrusive.parse("form");
}

function doStuff() {
    const apiUrl = '/ShowRoom/Refresh?showRoomType=1';
    $.ajax({
        type: 'GET',
        url: apiUrl,
    }).done(function (result) {
        if (result) {
            $(".comment-list").html(result);
        } else {
            $(".comment-list").html('<li class="comment byuser comment-author-admin bypostauthor even thread-even depth-1" id="li-comment-3">尚未有任何留言</li>');
        }
    }).fail(function () {
        console.log("錯誤");
    });
}
